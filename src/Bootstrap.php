<?php

/**
 * @package:    foursites-wordpress-admin-pages
 * @author:     Harm Putman <harm.putman@foursites.nl>
 * @copyright:  2020 - Foursites
 *
 * Created:     2020-08-07, 02:57:15 pm
 * Modified:    2020-08-10, 04:22:47 pm
 * Modified By: Harm Putman <harm.putman@foursites.nl>
 */

namespace Foursites\WordPressAdminPages;

defined('ABSPATH') or die('These are not the droids you are looking for...');

final class Bootstrap
{
    protected $pages = [];

    protected $subpages = [];

    public function register()
    {
        add_action('admin_menu', [$this, 'addPages']);
    }

    public function addPages()
    {
        $this->build();
        $this->registerPages();
        $this->registerSubPages();
    }

    public static function isPluginPage()
    {
        global $pagenow;

        if (!is_admin() || 'admin.php' !== $pagenow || !isset($_GET['page'])) {
            return false;
        }

        $pages = new static();

        return in_array($_GET['page'], $pages->getSlugs());
    }

    protected function build()
    {
        $this->buildPagesArray();
        $this->buildSubPagesArray();
    }

    protected function buildPagesArray()
    {
        $filter      = apply_filters('foursites_wordpress_admin_pages_filter_name', 'fs_wp_pages');
        $this->pages = apply_filters($filter, []);

        foreach ($this->pages as $page) {
            if (isset($page['sub_page_title']) && $page['sub_page_title']) {
                $this->subpages[] = $this->createSubPageFrom($page, $page['sub_page_title']);
            }
        }
    }

    protected function createSubPageFrom($page = [], $title = '')
    {
        $page['parent_slug'] = $page['menu_slug'];
        $page['menu_title']  = $title ?: $page['menu_title'];
        unset($page['icon_url'], $page['position']);

        return $page;
    }

    protected function buildSubPagesArray()
    {
        $filter         = apply_filters('foursites_wordpress_admin_subpages_filter_name', 'fs_wp_subpages');
        $this->subpages = array_merge($this->subpages, apply_filters($filter, []));
    }

    protected function registerPages()
    {
        if (!$this->getPages()) {
            return false;
        }

        foreach ($this->getPages() as $page) {
            add_menu_page(
                $page['page_title'],
                $page['menu_title'],
                $page['capability'],
                $page['menu_slug'],
                $page['callback'],
                $page['icon_url'],
                $page['position']
            );
        }
    }

    protected function registerSubPages()
    {
        if (!$this->getSubPages()) {
            return false;
        }

        foreach ($this->getSubPages() as $page) {
            add_submenu_page(
                $page['parent_slug'],
                $page['page_title'],
                $page['menu_title'],
                $page['capability'],
                $page['menu_slug'],
                $page['callback']
            );
        }
    }

    protected function getPages()
    {
        return $this->pages;
    }

    protected function getSubPages()
    {
        return $this->subpages;
    }

    protected function getSlugs()
    {
        $this->build();

        return array_merge(array_column($this->getPages(), 'menu_slug'), array_column($this->getSubPages(), 'menu_slug'));
    }
}
